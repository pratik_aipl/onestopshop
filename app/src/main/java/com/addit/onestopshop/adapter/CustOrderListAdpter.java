package com.addit.onestopshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.addit.onestopshop.R;
import com.addit.onestopshop.activity.CustomerOrderDetailsList;
import com.addit.onestopshop.model.CutOrderListModel;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CustOrderListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<CutOrderListModel> singleitemList;

    public CustOrderListAdpter(Context context, List<CutOrderListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.custorderlistrow, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        CutOrderListModel listModel = singleitemList.get(position);

        holder.tvOrderId.setText(listModel.getOrderNumber());
        holder.tvDate.setText(L.getDate(listModel.getCreatedOn()));
        holder.tvStatus.setText(listModel.getOrder_Status());
        holder.tvTotal.setText("Rs "+listModel.getOrder_Item_Price());

        if(listModel.getOrder_Status().equalsIgnoreCase("Pending")){
            holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.yallow));
        }else if(listModel.getOrder_Status().equalsIgnoreCase("Partially Complete")){
            holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.green));
        }else if(listModel.getOrder_Status().equalsIgnoreCase("Complete")){
            holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.green));
        }else{
            holder.tvStatus.setTextColor(ContextCompat.getColor(context, R.color.red));
        }

        holder.itemView.setOnClickListener(view ->
                context.startActivity(new Intent(context, CustomerOrderDetailsList.class)
                        .putExtra(Constant.ORDERDETAILS, listModel)));


    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_orderId)
        TextView tvOrderId;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_total)
        TextView tvTotal;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}