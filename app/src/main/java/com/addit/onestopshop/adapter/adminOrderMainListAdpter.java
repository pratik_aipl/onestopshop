package com.addit.onestopshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.addit.onestopshop.R;
import com.addit.onestopshop.activity.AdminOrderDetailsList;
import com.addit.onestopshop.activity.CustomerOrderDetailsList;
import com.addit.onestopshop.listner.OrderStatusUpdate;
import com.addit.onestopshop.model.CutOrderListModel;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class adminOrderMainListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<CutOrderListModel> singleitemList;
    List<CutOrderListModel> singleitemListCopy;
    adminOrderItemListAdpter adminOrderListAdpter;
    String TYPE,STATUS;
    OrderStatusUpdate orderStatusUpdate;

    public adminOrderMainListAdpter(Context context, List<CutOrderListModel> singleitemLists, String TYPE) {
        this.context = context;
        this.singleitemList = singleitemLists;
        this.singleitemListCopy = singleitemLists;
        this.TYPE = TYPE;
        this.orderStatusUpdate = (OrderStatusUpdate) context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adminmainorderlistrow, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        CutOrderListModel listModel = singleitemListCopy.get(position);


        holder.tvOrderId.setText(listModel.getOrderNumber());
        holder.tvDate.setText(L.getDate(listModel.getCreatedOn()));
        holder.tvTotal.setText("Rs " + listModel.getOrder_Item_Price());

       /* holder.orderlist.setHasFixedSize(true);
        holder.orderlist.setItemAnimator(new DefaultItemAnimator());
        holder.orderlist.setLayoutManager(new LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL, false));

        adminOrderListAdpter = new adminOrderItemListAdpter(context, listModel.getOrder_details());
        holder.orderlist.setAdapter(adminOrderListAdpter);*/


        Log.d(TAG, "onBindViewHolder: "+TYPE);
        if (TYPE.equalsIgnoreCase("Customer")) {
            holder.liUserQTY.setVisibility(View.GONE);
            holder.btnSubmit.setVisibility(View.GONE);
            holder.tv_status.setVisibility(View.VISIBLE);
            holder.tv_cust_name.setVisibility(View.GONE);
            holder.btn_view_more.setVisibility(View.GONE);

            holder.tv_status.setText(listModel.getOrder_Status());
            if(listModel.getOrder_Status().equalsIgnoreCase("Pending")){
                holder.tv_status.setTextColor(ContextCompat.getColor(context, R.color.yallow));
            }else if(listModel.getOrder_Status().equalsIgnoreCase("Complete")){
                holder.tv_status.setTextColor(ContextCompat.getColor(context, R.color.green));
            }else{
                holder.tv_status.setTextColor(ContextCompat.getColor(context, R.color.red));
            }

        }else{
            holder.liUserQTY.setVisibility(View.VISIBLE);
            holder.btnSubmit.setVisibility(View.GONE);
            holder.tv_status.setVisibility(View.GONE);
            holder.tv_cust_name.setVisibility(View.VISIBLE);
            holder.btn_view_more.setVisibility(View.VISIBLE);

            holder.spnUserQty.setEnabled(false);
            holder.tv_cust_name.setText(listModel.getCustomerName());
            if(listModel.getOrder_Status().equalsIgnoreCase("Pending")){
                holder.spnUserQty.setSelection(0);
            }else if(listModel.getOrder_Status().equalsIgnoreCase("Partially_Complete")){
                //holder.tv_status.setTextColor(ContextCompat.getColor(context, R.color.green));
                holder.spnUserQty.setSelection(2);
            }else if(listModel.getOrder_Status().equalsIgnoreCase("Complete")){
                holder.spnUserQty.setSelection(3);
            }else{
                holder.spnUserQty.setSelection(1);
            }
        }


       holder.spnUserQty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                if(position==0){
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#FF9800"));
                }else if(position==1){
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#FADD1919"));
                }else if(position==2){
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#4CAF50"));
                }else if(position==3){
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#4CAF50"));
                }else{

                }
                STATUS = holder.spnUserQty.getSelectedItem().toString();
               // Toast.makeText(context, ""+STATUS, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        holder.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderStatusUpdate.onUpdatedCart(listModel.getOrderID(),STATUS);
            }
        });
        holder.btn_view_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, AdminOrderDetailsList.class)
                        .putExtra(Constant.ORDERDETAILS, listModel));

            }
        });

    }
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    singleitemListCopy = singleitemList;
                } else {
                    List<CutOrderListModel> filteredList = new ArrayList<>();
                    for (CutOrderListModel row : singleitemList) {
                        if (row.getOrderNumber().toLowerCase().contains(charString.toLowerCase())
                                ||row.getOrder_Status().toLowerCase().contains(charString.toLowerCase())
                                ||row.getCustomerName().toLowerCase().contains(charString.toLowerCase())
                                ||row.getCreatedOn().toLowerCase().contains(charString.toLowerCase())
                                ||row.getOrder_Item_Price().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    singleitemListCopy = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = singleitemListCopy;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                singleitemListCopy = (ArrayList<CutOrderListModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    @Override
    public int getItemCount() {
        return singleitemListCopy.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_orderId)
        TextView tvOrderId;
        @BindView(R.id.tv_status)
        TextView tv_status;
        @BindView(R.id.spn_UserQty)
        Spinner spnUserQty;
        @BindView(R.id.li_user_QTY)
        LinearLayout liUserQTY;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_cust_name)
        TextView tv_cust_name;
        @BindView(R.id.tv_total)
        TextView tvTotal;
        @BindView(R.id.btn_submit)
        Button btnSubmit;
        @BindView(R.id.btn_view_more)
        Button btn_view_more;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}