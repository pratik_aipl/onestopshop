package com.addit.onestopshop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.onestopshop.R;
import com.addit.onestopshop.model.CutOrderDetailsListModel;
import com.addit.onestopshop.model.ItemListDetailsModel;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class adminOrderItemDelivryListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<CutOrderDetailsListModel> singleitemList;

    int minteger;
    ProductsListAdapter productsListAdapter;
    String PPRice,LPID,UOMQty,TYPE;


    public adminOrderItemDelivryListAdpter(Context context, List<CutOrderDetailsListModel> singleitemLists, String TYPE) {
        this.context = context;
        this.singleitemList = singleitemLists;
        this.TYPE = TYPE;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.orderitemdelivrylistrow, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        CutOrderDetailsListModel listModel = singleitemList.get(position);

        holder.tvOrderId.setText(listModel.getProductName());

        if (listModel.getIsPack().equalsIgnoreCase("0")) {
            holder.liUserQTY.setVisibility(View.VISIBLE);
            holder.linCount.setVisibility(View.GONE);
            productsListAdapter = new ProductsListAdapter(context, listModel.getProduct_detail());
            holder.spnUserQty.setAdapter(productsListAdapter);
            for (int i = 0; i <listModel.getProduct_detail().size() ; i++) {
                Log.d(TAG, "getPktsize: "+listModel.getProduct_detail().get(i).getPktsize());
                Log.d(TAG, "getDUOMQty: "+listModel.getPendingUOMQty());
                if(listModel.getProduct_detail().get(i).getPktsize().equalsIgnoreCase(listModel.getPendingUOMQty())){
                    holder.spnUserQty.setSelection(i+1);
                }
            }
        } else {
            holder.liUserQTY.setVisibility(View.GONE);
            holder.linCount.setVisibility(View.VISIBLE);
            listModel.setDQTY(Integer.parseInt(listModel.getPendingQty()));
            if(TYPE.equalsIgnoreCase("Customer")){
                holder.edtQcount.setText(listModel.getPendingQty().equalsIgnoreCase(listModel.getQty())?"0":listModel.getPendingQty());
            }else{
                holder.edtQcount.setText(listModel.getPendingQty());

            }
        }

        if (TYPE.equalsIgnoreCase("Customer")) {
            holder.edtQcount.setClickable(false);
            holder.imgQminus.setVisibility(View.GONE);
            holder.imgQplus.setVisibility(View.GONE);
            holder.spnUserQty.setEnabled(false);
        }else{
            holder.edtQcount.setClickable(true);
            holder.imgQminus.setVisibility(View.VISIBLE);
            holder.imgQplus.setVisibility(View.VISIBLE);
            holder.spnUserQty.setEnabled(true);
        }

            holder.imgQminus.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(L.getEditText(holder.edtQcount)) && Integer.parseInt(holder.edtQcount.getText().toString().trim()) > 0) {
                minteger = Integer.parseInt(holder.edtQcount.getText().toString().trim());
                if (minteger != 0) {
                    minteger = Integer.parseInt(holder.edtQcount.getText().toString().trim());
                    minteger = minteger - 1;
                }
                holder.edtQcount.setText(context.getString(R.string.qty, minteger));
                for (int i = 0; i < singleitemList.size(); i++) {
                    if (singleitemList.get(i).getProductID().equals(listModel.getProductID())) {
                        listModel.setDQTY(minteger);
                    }
                }
            }

        });
            holder.imgQplus.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(L.getEditText(holder.edtQcount))) {
                minteger = Integer.parseInt(holder.edtQcount.getText().toString().trim());
                if(Integer.parseInt(listModel.getPendingQty())>minteger){
                    minteger = minteger + 1;
                    holder.edtQcount.setText(context.getString(R.string.qty, minteger));
                    for (int i = 0; i < singleitemList.size(); i++) {
                        if (singleitemList.get(i).getProductID().equals(listModel.getProductID())) {
                            listModel.setDQTY(minteger);
                        }
                    }
                }else{
                    Toast.makeText(context, "Maximum QTY is "+listModel.getPendingQty(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.spnUserQty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    PPRice = listModel.getProduct_detail().get(position - 1).getPrice();
                    LPID = listModel.getProduct_detail().get(position - 1).getUid();
                    UOMQty =listModel.getProduct_detail().get(position - 1).getPktsize();
                    for (int i = 0; i < singleitemList.size(); i++) {
                        if (singleitemList.get(i).getProductID().equals(listModel.getProductID())) {
                            listModel.setDLPPrice(PPRice);
                            listModel.setDLPID(LPID);
                            listModel.setDUOMQty(UOMQty);
                        }
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_orderId)
        TextView tvOrderId;
        @BindView(R.id.img_Qminus)
        ImageView imgQminus;
        @BindView(R.id.edt_Qcount)
        EditText edtQcount;
        @BindView(R.id.img_Qplus)
        ImageView imgQplus;
        @BindView(R.id.lin_count)
        LinearLayout linCount;
        @BindView(R.id.spn_UserQty)
        Spinner spnUserQty;
        @BindView(R.id.li_user_QTY)
        LinearLayout liUserQTY;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
    static class ProductsListAdapter extends BaseAdapter {
        List<ItemListDetailsModel> Productslist;
        LayoutInflater inflter;
        Context context;

        public ProductsListAdapter(Context context, List<ItemListDetailsModel> Productlists) {
            this.context = context;
            this.Productslist = Productlists;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            return Productslist.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return Productslist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Select QTY");
                names.setTextColor(Color.GRAY);
            } else {
                ItemListDetailsModel dtptcCMPYModel = Productslist.get(position - 1);
                names.setText(dtptcCMPYModel.getPktsize()+" "+dtptcCMPYModel.getUomdesc());
                names.setTextColor(Color.BLACK);
            }
            return convertView;
        }
    }
}