package com.addit.onestopshop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.onestopshop.R;
import com.addit.onestopshop.listner.CartCountEvent;
import com.addit.onestopshop.model.ItemListDetailsModel;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.addit.onestopshop.utils.Prefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class AdminProductItemListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    private Context context;
    private List<ItemListModel> singleitemList;
    public AdminProductItemListAdpter(Context context, List<ItemListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adminproductitemlistrow, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        /*ispck=0 (packed),isPack=1 (looss) */

        ViewHolder holder = (ViewHolder) holderIn;
        ItemListModel listModel = singleitemList.get(position);

        holder.proName.setText(listModel.getProductName());
        holder.btnAddcart.setVisibility(View.GONE);
        holder.linDis_qty.setVisibility(View.GONE);

        if(listModel.getActivated().equalsIgnoreCase("1")){
            holder.tvDisPrice.setVisibility(View.VISIBLE);
            holder.tvDisPrice.setText("RS " + listModel.getDis_price());
            holder.tvDisPrice.setPaintFlags(holder.tvDisPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }else{
            holder.tvDisPrice.setVisibility(View.GONE);
        }

        holder.tvPrice.setText("RS " + listModel.getPrice());
        if (listModel.getProductImage() != null)
            L.loadImageWithPicasso(context, listModel.getProductImage(), holder.imgItem, null);



    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_item)
        ImageView imgItem;
        @BindView(R.id.pro_name)
        TextView proName;
        @BindView(R.id.tv_dis_price)
        TextView tvDisPrice;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.btn_addcart)
        Button btnAddcart;
        @BindView(R.id.lin_dis_qty)
        LinearLayout linDis_qty;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
