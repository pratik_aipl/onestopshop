package com.addit.onestopshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.addit.onestopshop.R;
import com.addit.onestopshop.model.CutOrderDetailsListModel;
import com.addit.onestopshop.model.CutOrderListModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class adminOrderItemListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<CutOrderDetailsListModel> singleitemList;

    public adminOrderItemListAdpter(Context context, List<CutOrderDetailsListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.orderitemlistrow, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        CutOrderDetailsListModel listModel = singleitemList.get(position);

        holder.tvOrderId.setText(listModel.getProductName());
        if(listModel.getIsPack().equalsIgnoreCase("0")){
            holder.tvStatus.setText(listModel.getUOMQty()+" "+listModel.getUOMDesc()+" - Rs "+listModel.getTotalPrice());
        }else{
            holder.tvStatus.setText("Rs "+listModel.getUnitPrice()+" * "+listModel.getQty()+" QTY");
        }

    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_orderId)
        TextView tvOrderId;
        @BindView(R.id.tv_status)
        TextView tvStatus;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}