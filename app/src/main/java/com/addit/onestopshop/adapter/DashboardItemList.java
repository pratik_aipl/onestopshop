package com.addit.onestopshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.addit.onestopshop.R;
import com.addit.onestopshop.activity.ProductsList;
import com.addit.onestopshop.model.CatListModel;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardItemList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<CatListModel> CatList;

    public DashboardItemList(Context context, List<CatListModel> CatLists) {
        this.context = context;
        this.CatList = CatLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlistrow, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        CatListModel listModel = CatList.get(position);
        holder.proName.setText(listModel.getName());
        if (listModel.getImage() != null)
            L.loadImageWithPicasso(context, listModel.getImage(), holder.imgItem, null);

        holder.itemView.setOnClickListener(view ->
                context.startActivity(new Intent(context, ProductsList.class)
                        .putExtra(Constant.CATEGORYID, listModel.getCategoryID())
                        .putExtra(Constant.CATNAME, listModel.getName())));


    }

    @Override
    public int getItemCount() {
        return CatList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_item)
        ImageView imgItem;
        @BindView(R.id.pro_name)
        TextView proName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}