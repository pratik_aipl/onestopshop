package com.addit.onestopshop.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.onestopshop.R;
import com.addit.onestopshop.activity.CartActivity;
import com.addit.onestopshop.listner.CartCountEvent;
import com.addit.onestopshop.listner.GetUpdatedCart;
import com.addit.onestopshop.model.ItemListDetailsModel;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.addit.onestopshop.utils.Prefs;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CartProductsAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "CartProductsAdpater";
    Context context;
    List<ItemListModel> cartlist;
    int minteger;
    public Prefs prefs;
    protected Gson gson;
    GetUpdatedCart getUpdatedCart;
    //  RemoveProduct removeProduct;
    ProductsListAdapter productsListAdapter;
    String PPRice,LPID,UOMQty;

    public CartProductsAdpater(CartActivity context, List<ItemListModel> cartlists) {
        this.context = context;
        this.cartlist = cartlists;
        prefs = Prefs.with(context);
        gson = new Gson();
        getUpdatedCart=context;
        //   removeProduct = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.productitemlistrow, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        ItemListModel cartitem = cartlist.get(position);

        holder.btnAddcart.setText("REMOVE");

        if (cartitem.getProductImage()!= null) {
            L.loadImageWithPicasso(context, cartitem.getProductImage(), holder.imgItem, null);
        }

        holder.proName.setText(cartitem.getProductName());
        holder.tvPrice.setText("RS " + cartitem.getPrice());


        if(cartitem.getIsPack().equalsIgnoreCase("0")){
            holder.liUserQTY.setVisibility(View.VISIBLE);
            holder.linCount.setVisibility(View.GONE);
            holder.tv_perPrice.setVisibility(View.VISIBLE);
            holder.tv_per_qty.setVisibility(View.GONE);
            productsListAdapter = new ProductsListAdapter(context, cartitem.getDetails());
            holder.spnUserQty.setAdapter(productsListAdapter);
            holder.tv_perPrice.setText("RS "+cartitem.getLPPrice());
            for (int i = 0; i <cartitem.getDetails().size() ; i++) {
                if(cartitem.getDetails().get(i).getPktsize().equalsIgnoreCase(cartitem.getUOMQty())){
                    holder.spnUserQty.setSelection(i+1);
                }
            }

        }else{
            holder.liUserQTY.setVisibility(View.GONE);
            holder.tv_perPrice.setVisibility(View.GONE);
            holder.tv_per_qty.setVisibility(View.VISIBLE);
            holder.linCount.setVisibility(View.VISIBLE);
            holder.tv_per_qty.setText("RS "+(Double.parseDouble(cartitem.getPrice()) * cartitem.getQty()));

        }
        holder.edtQcount.setText(context.getString(R.string.qty, cartitem.getQty()));

        L.hideKeyboard(context, holder.edtQcount);
        holder.edtQcount.clearFocus();


        holder.imgQminus.setOnClickListener(view -> {
            if (TextUtils.isEmpty(L.getEditText(holder.edtQcount))) {
                Toast.makeText(context, "Please Enter Valid QTY", Toast.LENGTH_SHORT).show();
            } else if (holder.edtQcount.getText().toString().equalsIgnoreCase("0")) {
                Toast.makeText(context, "Please Enter Valid QTY", Toast.LENGTH_SHORT).show();
            }

            if (!TextUtils.isEmpty(L.getEditText(holder.edtQcount)) && Integer.parseInt(holder.edtQcount.getText().toString().trim()) > 1) {
                if (minteger != 1) {
                    minteger = Integer.parseInt(holder.edtQcount.getText().toString().trim());
                    minteger = minteger - 1;
                }
                holder.edtQcount.setText(context.getString(R.string.qty, minteger));

                for (int i = 0; i < cartlist.size(); i++) {
                    if (cartlist.get(i).getProductID().equals(cartitem.getProductID())) {
                        cartitem.setQty(minteger);
                    }
                }
                prefs.save(Constant.cartData, gson.toJson(cartlist));
                holder.edtQcount.clearFocus();
                L.hideKeyboard(context, holder.edtQcount);
                holder.tv_per_qty.setText("RS "+(Double.parseDouble(cartitem.getPrice()) * minteger));
                getUpdatedCart.onUpdatedCart();
            }


        });
        holder.imgQplus.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(L.getEditText(holder.edtQcount))) {
                minteger = Integer.parseInt(holder.edtQcount.getText().toString().trim());
                minteger = minteger + 1;
                holder.edtQcount.setText(context.getString(R.string.qty, minteger));
                for (int i = 0; i < cartlist.size(); i++) {
                    if (cartlist.get(i).getProductID().equals(cartitem.getProductID())) {
                        cartitem.setQty(minteger);
                    }
                }

                prefs.save(Constant.cartData, gson.toJson(cartlist));
                holder.edtQcount.clearFocus();
                L.hideKeyboard(context, holder.edtQcount);
                holder.tv_per_qty.setText("RS "+(Double.parseDouble(cartitem.getPrice()) * minteger));
                getUpdatedCart.onUpdatedCart();

            }

        });

        holder.btnAddcart.setOnClickListener(view -> new AlertDialog.Builder(context)
                .setMessage(("Are you sure to Remove?"))
                .setPositiveButton("YES", (dialog, which) -> {
                    cartlist.remove(position);
                    notifyDataSetChanged();
                    EventBus.getDefault().post(new CartCountEvent(cartlist.size()));
                    prefs.save(Constant.cartData, gson.toJson(cartlist));
                    getUpdatedCart.onUpdatedCart();

                })
                .setNegativeButton("NO", null)
                .show());

        holder.spnUserQty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    PPRice = cartitem.getDetails().get(position - 1).getPrice();
                    LPID = cartitem.getDetails().get(position - 1).getUid();
                    UOMQty = cartitem.getDetails().get(position - 1).getPktsize();
                    holder.tv_perPrice.setText("RS "+PPRice);
                    for (int i = 0; i < cartlist.size(); i++) {
                        if (cartlist.get(i).getProductID().equals(cartitem.getProductID())) {
                            cartitem.setLPPrice(PPRice);
                            cartitem.setLPID(LPID);
                            cartitem.setUOMQty(UOMQty);
                        }
                    }
                    prefs.save(Constant.cartData, gson.toJson(cartlist));
                    getUpdatedCart.onUpdatedCart();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartlist.size();
    }


    static
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_item)
        ImageView imgItem;
        @BindView(R.id.pro_name)
        TextView proName;
        @BindView(R.id.tv_dis_price)
        TextView tvDisPrice;
        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.btn_addcart)
        Button btnAddcart;

        @BindView(R.id.img_Qminus)
        ImageView imgQminus;
        @BindView(R.id.img_Qplus)
        ImageView imgQplus;
        @BindView(R.id.edt_Qcount)
        EditText edtQcount;
        @BindView(R.id.tv_per_price)
        TextView tv_perPrice;
        @BindView(R.id.tv_per_qty)
        TextView tv_per_qty;
        @BindView(R.id.spn_UserQty)
        Spinner spnUserQty;
        @BindView(R.id.li_user_QTY)
        LinearLayout liUserQTY;
        @BindView(R.id.lin_count)
        LinearLayout linCount;
        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
    static class ProductsListAdapter extends BaseAdapter {
        List<ItemListDetailsModel> Productslist;
        LayoutInflater inflter;
        Context context;

        public ProductsListAdapter(Context context, List<ItemListDetailsModel> Productlists) {
            this.context = context;
            this.Productslist = Productlists;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            return Productslist.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return Productslist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Select QTY");
                names.setTextColor(Color.GRAY);
            } else {
                ItemListDetailsModel dtptcCMPYModel = Productslist.get(position - 1);
                names.setText(dtptcCMPYModel.getPktsize()+" "+dtptcCMPYModel.getUomdesc());
                names.setTextColor(Color.BLACK);
            }
            return convertView;
        }
    }
}