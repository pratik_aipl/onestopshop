package com.addit.onestopshop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.onestopshop.R;
import com.addit.onestopshop.activity.ProductsList;
import com.addit.onestopshop.listner.CartCountEvent;
import com.addit.onestopshop.model.ItemListDetailsModel;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.addit.onestopshop.utils.Prefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class ProductItemListAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    private Context context;
    private List<ItemListModel> singleitemList;
    private int minteger;
    private Subscription subscription;
    public Prefs prefs;
    protected Gson gson;
    public Type type;
    String PPRice,LPID,UOMQty;
    ProductsListAdapter productsListAdapter;
    public ProductItemListAdpter(Context context, List<ItemListModel> singleitemLists) {
        this.context = context;
        this.singleitemList = singleitemLists;
        prefs = Prefs.with(context);
        gson = new Gson();
        type = new TypeToken<List<ItemListModel>>() {
        }.getType();

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.productitemlistrow, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        /*ispck=0 (packed),isPack=1 (looss) */

        ViewHolder holder = (ViewHolder) holderIn;
        ItemListModel listModel = singleitemList.get(position);

        holder.proName.setText(listModel.getProductName());

        if(listModel.getActivated().equalsIgnoreCase("1")){
            holder.tvDisPrice.setVisibility(View.VISIBLE);
            holder.tvDisPrice.setText("RS " + listModel.getDis_price());
            holder.tvDisPrice.setPaintFlags(holder.tvDisPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }else{
            holder.tvDisPrice.setVisibility(View.GONE);

        }
        holder.tvPrice.setText("RS " + listModel.getPrice());
        if (listModel.getProductImage() != null)
            L.loadImageWithPicasso(context, listModel.getProductImage(), holder.imgItem, null);

        if(listModel.getIsPack().equalsIgnoreCase("0")){
            holder.liUserQTY.setVisibility(View.VISIBLE);
            holder.tv_perPrice.setVisibility(View.VISIBLE);
            holder.linCount.setVisibility(View.GONE);
            holder.tv_per_qty.setVisibility(View.GONE);
            productsListAdapter = new ProductsListAdapter(context, listModel.getDetails());
            holder.spnUserQty.setAdapter(productsListAdapter);
        }else{
            holder.liUserQTY.setVisibility(View.GONE);
            holder.tv_perPrice.setVisibility(View.GONE);
            holder.tv_per_qty.setVisibility(View.VISIBLE);
            holder.linCount.setVisibility(View.VISIBLE);
        }

        holder.imgQminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(L.getEditText(holder.edtQcount)) && Integer.parseInt(holder.edtQcount.getText().toString().trim()) > 1) {
                    if (minteger != 1) {
                        minteger = Integer.parseInt(holder.edtQcount.getText().toString().trim());
                        Log.d(TAG, "valuegetminus>>: " + minteger);
                        minteger = minteger - 1;
                    }

                    holder.edtQcount.setText(context.getString(R.string.qty, minteger));
                    holder.edtQcount.setSelection(holder.edtQcount.getText().length());
                    holder.tv_per_qty.setText("RS "+(Double.parseDouble(listModel.getPrice()) * minteger));

                }
            }
        });

        holder.imgQplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(L.getEditText(holder.edtQcount))) {
                    minteger = Integer.parseInt(holder.edtQcount.getText().toString().trim());
                    minteger = minteger + 1;
                    Log.d(TAG, "valuegetplus>>: " + minteger);
                    holder.edtQcount.setText(context.getString(R.string.qty, minteger));
                    holder.edtQcount.setSelection(holder.edtQcount.getText().length());
                    holder.tv_per_qty.setText("RS "+(Double.parseDouble(listModel.getPrice()) * minteger));


                }
            }
        });


        holder.btnAddcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(listModel.getIsPack().equalsIgnoreCase("0")){
                    //For LOOSE Products
                    if (listModel.getDetails().size() == 0 || holder.spnUserQty.getSelectedItemPosition() == 0){
                        Toast.makeText(context, "Please Select QTY", Toast.LENGTH_SHORT).show();
                    }else{
                            //  prefs.save(Constant.cartData, "");
                            listModel.setQty(Integer.parseInt(L.getText(holder.edtQcount)));
                            listModel.setLPPrice(PPRice);
                            listModel.setLPID(LPID);
                            listModel.setUOMQty(UOMQty);
                            List<ItemListModel> detailsModelList = new ArrayList<>();

                            if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
                                detailsModelList = gson.fromJson(prefs.getString(Constant.cartData, ""), type);
                            }
                            Log.d(TAG, "detailsModelList "+detailsModelList.toString());
                            Log.d(TAG, "cart data: "+prefs.getString(Constant.cartData, ""));
                            Log.d(TAG, "getProductID "+listModel.getProductID());

                            boolean isUpdate = false;
                            for (int i = 0; i < detailsModelList.size(); i++) {
                                if (listModel.getProductID().equals(detailsModelList.get(i).getProductID())) {
                                    detailsModelList.get(i).setQty(Integer.parseInt(L.getText(holder.edtQcount)));
                                    detailsModelList.get(i).setLPPrice(PPRice);
                                    detailsModelList.get(i).setLPID(LPID);
                                    detailsModelList.get(i).setLPID(UOMQty);

                                    isUpdate = true;
                                    break;
                                }
                            }

                            if (isUpdate) {
                                Toast.makeText(context, "Updated Successfully", Toast.LENGTH_SHORT).show();
                                //SuccessDialog(this, "Updated Successfully", true);
                                subscription = Observable
                                        .timer(1000, TimeUnit.MILLISECONDS) // 5000ms = 5s
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(aLong -> {
                                            // SuccessDialog(this, "", false);
                                        });
                            } else {
                                detailsModelList.add(listModel);
                                Toast.makeText(context, "Add To Cart Successfully", Toast.LENGTH_SHORT).show();
                                //SuccessDialog(this, "Added Successfully", true);
                                subscription = Observable
                                        .timer(1000, TimeUnit.MILLISECONDS) // 5000ms = 5s
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(aLong -> {
                                            // SuccessDialog(this, "", false);
                                        });
                            }
                            holder.edtQcount.clearFocus();
                            prefs.save(Constant.cartData, gson.toJson(detailsModelList));
                            EventBus.getDefault().post(new CartCountEvent(detailsModelList.size()));
                    }

                }else{
                    //For FIX Products
                    if (TextUtils.isEmpty(L.getEditText(holder.edtQcount))) {
                        Toast.makeText(context, "Please Enter Valid QTY", Toast.LENGTH_SHORT).show();
                    } else if (holder.edtQcount.getText().toString().equalsIgnoreCase("0")) {
                        Toast.makeText(context, "Please Enter Valid QTY", Toast.LENGTH_SHORT).show();
                    } else {
                        //  prefs.save(Constant.cartData, "");
                        listModel.setQty(Integer.parseInt(L.getText(holder.edtQcount)));
                        List<ItemListModel> detailsModelList = new ArrayList<>();

                        if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
                            detailsModelList = gson.fromJson(prefs.getString(Constant.cartData, ""), type);
                        }
                        Log.d(TAG, "detailsModelList "+detailsModelList.toString());
                        Log.d(TAG, "cart data: "+prefs.getString(Constant.cartData, ""));
                        Log.d(TAG, "getProductID "+listModel.getProductID());

                        boolean isUpdate = false;
                        for (int i = 0; i < detailsModelList.size(); i++) {
                            if (listModel.getProductID().equals(detailsModelList.get(i).getProductID())) {
                                detailsModelList.get(i).setQty(Integer.parseInt(L.getText(holder.edtQcount)));
                                isUpdate = true;
                                break;
                            }
                        }

                        if (isUpdate) {
                            Toast.makeText(context, "Updated Successfully", Toast.LENGTH_SHORT).show();
                            //SuccessDialog(this, "Updated Successfully", true);
                            subscription = Observable
                                    .timer(1000, TimeUnit.MILLISECONDS) // 5000ms = 5s
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(aLong -> {
                                        // SuccessDialog(this, "", false);
                                    });
                        } else {
                            detailsModelList.add(listModel);
                            Toast.makeText(context, "Add To Cart Successfully", Toast.LENGTH_SHORT).show();
                            //SuccessDialog(this, "Added Successfully", true);
                            subscription = Observable
                                    .timer(1000, TimeUnit.MILLISECONDS) // 5000ms = 5s
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(aLong -> {
                                        // SuccessDialog(this, "", false);
                                    });
                        }
                        holder.edtQcount.clearFocus();
                        prefs.save(Constant.cartData, gson.toJson(detailsModelList));
                        EventBus.getDefault().post(new CartCountEvent(detailsModelList.size()));
                    }
                }

            }
        });


        holder.spnUserQty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    PPRice = listModel.getDetails().get(position - 1).getPrice();
                    LPID = listModel.getDetails().get(position - 1).getUid();
                    UOMQty = listModel.getDetails().get(position - 1).getPktsize();
                    holder.tv_perPrice.setText("RS "+PPRice);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public int getItemCount() {
        return singleitemList.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_item)
        ImageView imgItem;
        @BindView(R.id.pro_name)
        TextView proName;
        @BindView(R.id.tv_dis_price)
        TextView tvDisPrice;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_per_price)
        TextView tv_perPrice;
        @BindView(R.id.tv_per_qty)
        TextView tv_per_qty;

        @BindView(R.id.btn_addcart)
        Button btnAddcart;

        @BindView(R.id.img_Qminus)
        ImageView imgQminus;
        @BindView(R.id.img_Qplus)
        ImageView imgQplus;
        @BindView(R.id.edt_Qcount)
        EditText edtQcount;

        @BindView(R.id.spn_UserQty)
        Spinner spnUserQty;
        @BindView(R.id.li_user_QTY)
        LinearLayout liUserQTY;
        @BindView(R.id.lin_count)
        LinearLayout linCount;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
    static class ProductsListAdapter extends BaseAdapter {
        List<ItemListDetailsModel> Productslist;
        LayoutInflater inflter;
        Context context;

        public ProductsListAdapter(Context context, List<ItemListDetailsModel> Productlists) {
            this.context = context;
            this.Productslist = Productlists;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            return Productslist.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return Productslist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Select QTY");
                names.setTextColor(Color.GRAY);
            } else {
                ItemListDetailsModel dtptcCMPYModel = Productslist.get(position - 1);
                names.setText(dtptcCMPYModel.getPktsize()+" "+dtptcCMPYModel.getUomdesc());
                names.setTextColor(Color.BLACK);
            }
            return convertView;
        }
    }

}
