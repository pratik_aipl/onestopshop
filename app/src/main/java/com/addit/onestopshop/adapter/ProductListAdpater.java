package com.addit.onestopshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.addit.onestopshop.R;
import com.addit.onestopshop.activity.ProductsItemList;
import com.addit.onestopshop.activity.ProductsList;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.model.SubCatListModel;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "MoreItemDashboardadpate";
    Context context;
    List<SubCatListModel> subCatList;
    String CatID;

    public ProductListAdpater(Context context, List<SubCatListModel> subCatLists,String CatID) {
        this.context = context;
        this.subCatList = subCatLists;
        this.CatID = CatID;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product_grid_view, parent, false));


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        SubCatListModel listModel = subCatList.get(position);

        holder.proName.setText(listModel.getSubcategoryName());
        if (listModel.getSubCategoryImage() != null)
            L.loadImageWithPicasso(context, listModel.getSubCategoryImage(), holder.imgItem, null);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ProductsItemList.class)
                        .putExtra(Constant.CATEGORYID, listModel.getCategoryID())
                        .putExtra(Constant.SUBCATEGORYID, listModel.getSubcategoryID())
                        .putExtra(Constant.CATNAME, listModel.getSubcategoryName()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return subCatList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_item)
        ImageView imgItem;
        @BindView(R.id.pro_name)
        TextView proName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}