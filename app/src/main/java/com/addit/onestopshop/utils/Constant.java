package com.addit.onestopshop.utils;

public class Constant {


    public static String file = "file";

    public static String UserData = "UserData";
    public static String image = "image";
    public static String video = "video";
    public static String audio = "audio";
    public static String content = "content";
    public static String HTTP = "http";
    public static String HTTPS = "https";
    public static final String DeviceID = "device_token";
    public static boolean isAlertShow = false;
    public static String message = "message";
    public static String isLogin = "isLogin";
    public static String loginAuthToken = "loginAuthToken";
    public static String USERID = "UserId";
    public static String USERNAME = "username";
    public static String data = "data";
    public static String CATEGORYID = "CategoryID";
    public static String SUBCATEGORYID = "subcategoryid";
    public static String CATNAME = "catname";
    public static String PRODUCTLIST = "ProductList";
    public static String ORDERDETAILS = "orderdetails";


    public static String EMAILID = "email";
    public static String USERName = "username";
    public static String SURNAME = "surname";
    public static String MOBILE = "mobile_no";
    public static String Password = "password";
    public static String GENDER = "gender";
    public static String DOB = "dob";


    public static String cartData="cartData";
}
