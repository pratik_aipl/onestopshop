package com.addit.onestopshop.utils;

import android.text.TextUtils;

public class Validation {

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
       }
    }

    public static boolean isValidPhoneNumber(CharSequence target) {
        if (target.length() != 10) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    public static boolean isEmpty(CharSequence target) {

        return TextUtils.isEmpty(target);
    }



}
