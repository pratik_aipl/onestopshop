package com.addit.onestopshop;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;


import com.addit.onestopshop.utils.Prefs;

import java.io.File;


public class OSSApplication extends Application {

    public static Context mContext;
    private Prefs prefs;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = this;
        prefs = Prefs.with(this);
      //  Fabric.with(this, new Crashlytics());

     /*       CalligraphyConfig.initDefault(
                    new CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/RALEWAY-REGULAR.TTF")
                            .setFontAttrId(R.attr.fontPath)
                            .build());*/
      /*  OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new NotificationOpenedHandler(getApplicationContext()))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();*/
    }

    @Override
    public void onTerminate() {
        android.os.Process.killProcess(android.os.Process.myPid());
        SharedPreferences.Editor editor = getSharedPreferences("clear_cache", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        trimCache(this);
        super.onTerminate();
    }


    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);

            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

}
