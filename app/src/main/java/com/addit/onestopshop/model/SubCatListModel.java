package com.addit.onestopshop.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class SubCatListModel implements Serializable {

    @JsonField
    String SubcategoryID;
    @JsonField
    String CategoryID;
    @JsonField
    String SubcategoryName;
    @JsonField
    String SubCategoryImage;

    public String getSubcategoryID() {
        return SubcategoryID;
    }

    public void setSubcategoryID(String subcategoryID) {
        SubcategoryID = subcategoryID;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }


    public String getSubcategoryName() {
        return SubcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        SubcategoryName = subcategoryName;
    }

    public String getSubCategoryImage() {
        return SubCategoryImage;
    }

    public void setSubCategoryImage(String subCategoryImage) {
        SubCategoryImage = subCategoryImage;
    }
}
