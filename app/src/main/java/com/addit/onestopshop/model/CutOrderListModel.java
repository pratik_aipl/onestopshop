package com.addit.onestopshop.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonObject
public class CutOrderListModel implements Serializable {


    @JsonField
    String OrderNumber;
    @JsonField
    String OrderID;
    @JsonField
    String UserID;
    @JsonField
    String SubTotal;
    @JsonField
    String Order_Item_Price;
    @JsonField
    String Out_Quantity;
    @JsonField
    String Order_Status;
    @JsonField
    String DeliveryCharge;
    @JsonField
    String CreatedBy;
    @JsonField
    String CreatedOn;
    @JsonField
    String ModifiedBy;
    @JsonField
    String CustomerName;
    @JsonField
    List<CutOrderDetailsListModel> Order_details;

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public List<CutOrderDetailsListModel> getOrder_details() {
        return Order_details;
    }

    public void setOrder_details(List<CutOrderDetailsListModel> order_details) {
        Order_details = order_details;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getSubTotal() {
        return SubTotal;
    }

    public void setSubTotal(String subTotal) {
        SubTotal = subTotal;
    }

    public String getOrder_Item_Price() {
        return Order_Item_Price;
    }

    public void setOrder_Item_Price(String order_Item_Price) {
        Order_Item_Price = order_Item_Price;
    }

    public String getOut_Quantity() {
        return Out_Quantity;
    }

    public void setOut_Quantity(String out_Quantity) {
        Out_Quantity = out_Quantity;
    }

    public String getOrder_Status() {
        return Order_Status;
    }

    public void setOrder_Status(String order_Status) {
        Order_Status = order_Status;
    }

    public String getDeliveryCharge() {
        return DeliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        DeliveryCharge = deliveryCharge;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }
}