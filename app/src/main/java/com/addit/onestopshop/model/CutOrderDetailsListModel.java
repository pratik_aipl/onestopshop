package com.addit.onestopshop.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class CutOrderDetailsListModel implements Serializable {

    @JsonField
    String OrderDetailID;
    @JsonField
    String ProductID;
    @JsonField
    String UnitPrice;
    @JsonField
    String Qty;
    @JsonField
    String UOMQty;
    @JsonField
    String TotalPrice;
    @JsonField
    String ProductName;
    @JsonField
    String ProductCode;
    @JsonField
    String ProductImage;
    @JsonField
    String isPack;
    @JsonField
    String PKTSize;
    @JsonField
    String UOMDesc;
    @JsonField
    String PendingQty;
    @JsonField
    String DeliveryQty;
    @JsonField
    String PendingUOMQty;
    @JsonField
    List<ItemListDetailsModel> Product_detail;

    int DQTY=0;
    String DLPPrice = "";
    String DLPID = "";
    String DUOMQty = "";


    public String getPendingUOMQty() {
        return PendingUOMQty;
    }

    public void setPendingUOMQty(String pendingUOMQty) {
        PendingUOMQty = pendingUOMQty;
    }

    public String getDeliveryQty() {
        return DeliveryQty;
    }

    public void setDeliveryQty(String deliveryQty) {
        DeliveryQty = deliveryQty;
    }

    public String getPendingQty() {
        return PendingQty;
    }

    public void setPendingQty(String pendingQty) {
        PendingQty = pendingQty;
    }

    public String getDLPPrice() {
        return DLPPrice;
    }

    public void setDLPPrice(String DLPPrice) {
        this.DLPPrice = DLPPrice;
    }

    public String getDLPID() {
        return DLPID;
    }

    public void setDLPID(String DLPID) {
        this.DLPID = DLPID;
    }

    public String getDUOMQty() {
        return DUOMQty;
    }

    public void setDUOMQty(String DUOMQty) {
        this.DUOMQty = DUOMQty;
    }

    public int getDQTY() {
        return DQTY;
    }

    public void setDQTY(int DQTY) {
        this.DQTY = DQTY;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public List<ItemListDetailsModel> getProduct_detail() {
        return Product_detail;
    }

    public void setProduct_detail(List<ItemListDetailsModel> product_detail) {
        Product_detail = product_detail;
    }

    public String getOrderDetailID() {
        return OrderDetailID;
    }

    public void setOrderDetailID(String orderDetailID) {
        OrderDetailID = orderDetailID;
    }

    public String getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        UnitPrice = unitPrice;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    public String getUOMQty() {
        return UOMQty;
    }

    public void setUOMQty(String UOMQty) {
        this.UOMQty = UOMQty;
    }

    public String getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        TotalPrice = totalPrice;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProductImage() {
        return ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }

    public String getIsPack() {
        return isPack;
    }

    public void setIsPack(String isPack) {
        this.isPack = isPack;
    }

    public String getPKTSize() {
        return PKTSize;
    }

    public void setPKTSize(String PKTSize) {
        this.PKTSize = PKTSize;
    }

    public String getUOMDesc() {
        return UOMDesc;
    }

    public void setUOMDesc(String UOMDesc) {
        this.UOMDesc = UOMDesc;
    }
}