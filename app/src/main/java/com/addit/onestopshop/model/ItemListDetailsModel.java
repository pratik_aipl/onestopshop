package com.addit.onestopshop.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class ItemListDetailsModel implements Serializable {

    @JsonField
    String productcode;
    @JsonField
    String pktsize;
    @JsonField
    String price;
    @JsonField
    String uid;
    @JsonField
    String uomdesc;

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public String getPktsize() {
        return pktsize;
    }

    public void setPktsize(String pktsize) {
        this.pktsize = pktsize;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUomdesc() {
        return uomdesc;
    }

    public void setUomdesc(String uomdesc) {
        this.uomdesc = uomdesc;
    }
}