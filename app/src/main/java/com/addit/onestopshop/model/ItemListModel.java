package com.addit.onestopshop.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonObject
public class ItemListModel implements Serializable {

    @JsonField
    String ProductID;
    @JsonField
    String CategoryID;
    @JsonField
    String SubcategoryID;
    @JsonField
    String UOMID;
    @JsonField
    String CategoryName;
    @JsonField
    String SubcategoryName;
    @JsonField
    String UOMDesc;
    @JsonField
    String ProductName;
    @JsonField
    String Price;
    @JsonField
    String Dis_price;
    @JsonField
    String ProductCode;
    @JsonField
    String isAvailable;
    @JsonField
    String PKTSize;
    @JsonField
    String isPack;
    @JsonField
    String activated;
    @JsonField
    String ProductImage;
    @JsonField
    List<ItemListDetailsModel> Details;

    int qty = 0;
    String LPPrice = "";
    String UOMQty = "";
    String LPID = "";
    boolean fromCart = false;

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public String getDis_price() {
        return Dis_price;
    }

    public void setDis_price(String dis_price) {
        Dis_price = dis_price;
    }

    public String getUOMQty() {
        return UOMQty;
    }

    public void setUOMQty(String UOMQty) {
        this.UOMQty = UOMQty;
    }

    public String getLPID() {
        return LPID;
    }

    public void setLPID(String LPID) {
        this.LPID = LPID;
    }

    public String getLPPrice() {
        return LPPrice;
    }

    public void setLPPrice(String LPPrice) {
        this.LPPrice = LPPrice;
    }

    public List<ItemListDetailsModel> getDetails() {
        return Details;
    }

    public void setDetails(List<ItemListDetailsModel> details) {
        Details = details;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String categoryID) {
        CategoryID = categoryID;
    }

    public String getSubcategoryID() {
        return SubcategoryID;
    }

    public void setSubcategoryID(String subcategoryID) {
        SubcategoryID = subcategoryID;
    }

    public String getUOMID() {
        return UOMID;
    }

    public void setUOMID(String UOMID) {
        this.UOMID = UOMID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getSubcategoryName() {
        return SubcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        SubcategoryName = subcategoryName;
    }

    public String getUOMDesc() {
        return UOMDesc;
    }

    public void setUOMDesc(String UOMDesc) {
        this.UOMDesc = UOMDesc;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(String isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getPKTSize() {
        return PKTSize;
    }

    public void setPKTSize(String PKTSize) {
        this.PKTSize = PKTSize;
    }

    public String getIsPack() {
        return isPack;
    }

    public void setIsPack(String isPack) {
        this.isPack = isPack;
    }

    public String getProductImage() {
        return ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public boolean isFromCart() {
        return fromCart;
    }

    public void setFromCart(boolean fromCart) {
        this.fromCart = fromCart;
    }
}