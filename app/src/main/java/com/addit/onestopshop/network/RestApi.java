package com.addit.onestopshop.network;


import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface RestApi {

    @FormUrlEncoded
    @POST("auth/login")
    Observable<Response<String>> getLogin(@FieldMap Map<String, String> stringMap);

     @FormUrlEncoded
    @POST("auth/register")
    Observable<Response<String>> getRegister(@FieldMap Map<String, String> stringMap);

     @FormUrlEncoded
    @POST("Category")
    Observable<Response<String>> GetCategory(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("product")
    Observable<Response<String>> GetSearchProduct(@FieldMap Map<String, String> stringMap);

     @FormUrlEncoded
    @POST("sub-category")
    Observable<Response<String>> GetSubCategory(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("product")
    Observable<Response<String>> GetProducts(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("Product/offer_product")
    Observable<Response<String>> GetOffersProducts(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("order/list")
    Observable<Response<String>> GetMyOrder(@FieldMap Map<String, String> stringMap);
    @FormUrlEncoded
    @POST("order/update_order")
    Observable<Response<String>> OrderStatusUpdate(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("auth/forgot_password")
    Observable<Response<String>> ForgotPasword(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("auth/logout")
    Observable<Response<String>> getLogout(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("Order")
    Observable<Response<String>> PlaceOrder(@FieldMap Map<String, String> stringMap);


    // ******************************

    @FormUrlEncoded
    @POST("cities")
    Observable<Response<String>> getCity(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("confirm")
    Observable<Response<String>> getverification(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("resendotp")
    Observable<Response<String>> getresendotp(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("distributor_list")
    Observable<Response<String>> getdistList(@FieldMap Map<String, String> stringMap);


    @Multipart
    @POST("update_Profile")
    Observable<Response<String>> updateProfile(@PartMap Map<String, RequestBody> params,
                                               @Part MultipartBody.Part files);

    @FormUrlEncoded
    @POST("place_order")
    Observable<Response<String>> placeOrder(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("get_offer_list")
    Observable<Response<String>> getOfferList(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("get_order_detail_list")
    Observable<Response<String>> getOrderDetails(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("get_dealer_dashboard_offer_list")
    Observable<Response<String>> getDashOfferList(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("{type}")
    Observable<Response<String>> getDealerReportList(@Path("type") String type, @FieldMap Map<String, String> stringMap);


    @FormUrlEncoded
    @POST("get_order_list")
    Observable<Response<String>> getOrdelList(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("get_event")
    Observable<Response<String>> getEventList(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("dealer_list")
    Observable<Response<String>> getDealerList(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("distributor_assign_dealer_list")
    Observable<Response<String>> getAssginDealerList(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("status")
    Observable<Response<String>> getStatusList(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("report")
    Observable<Response<String>> getDealerReport(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("dealer_wise")
    Observable<Response<String>> getDealerWiseReport(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("product_wise")
    Observable<Response<String>> getProductWiseReport(@FieldMap Map<String, String> stringMap);


    @FormUrlEncoded
    @POST("status_changed")
    Observable<Response<String>> ChangeOrderStatus(@FieldMap Map<String, String> stringMap);
    //-----------------------------------------------------------------------------------------------------------

    @FormUrlEncoded
    @POST("categories/shop_category")
    Observable<Response<String>> getShopMallCat(@FieldMap Map<String, String> stringMap);

//    @FormUrlEncoded
//    @POST("products/add_product")status
//    Observable<Response<String>> uploadProduct(@FieldMap Map<String, String> stringMap);

    @Multipart
    @POST("products/add_product")
    Observable<Response<String>> uploadProduct(@PartMap Map<String, RequestBody> params,
                                               @Part List<MultipartBody.Part> files);


    @FormUrlEncoded
    @POST("products/edit_product")
    Observable<Response<String>> editProduct(@FieldMap Map<String, String> stringMap);


    @GET("shop_detail/shop/{Id}")
    Observable<Response<String>> getShopList(@Path("Id") int ShopCategoryId);

    @GET("products/product_inquiry/")
    Observable<Response<String>> addProductsInq(@Query("ProID") int ProId);

    @GET("products/fields/{Id}")
    Observable<Response<String>> getFiledList(@Path("Id") int SubCatId);

    @GET("dashboard/my_inquiry")
    Observable<Response<String>> getMyInquiry();

    @GET("logout_advt/lg_advt")
    Observable<Response<String>> getExitads();

    @GET("dashboard/seller")
    Observable<Response<String>> getBuyerInquiry();

    @GET("products/my_product")
    Observable<Response<String>> getMyProducts();

    @GET("settings/remove_account")
    Observable<Response<String>> removeAccount();


    @FormUrlEncoded
    @POST("profile_update/setting_notification")
    Observable<Response<String>> notificationStatus(@FieldMap Map<String, String> stringMap);

    //    @Multipart
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @HTTP(method = "DELETE", path = "peep/", hasBody = true)
    Observable<Response<String>> peepDelete(@Body RequestBody object);

    String MULTIPART_FORM_DATA = "multipart/form-data";

    static RequestBody createRequestBody(@NonNull String s) {
        return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), s);
    }

}
