package com.addit.onestopshop.network;



import com.addit.onestopshop.OSSApplication;
import com.addit.onestopshop.utils.L;

import java.util.concurrent.TimeUnit;
import com.addit.onestopshop.BuildConfig;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

public class RestAPIBuilder {

    private static final String TAG = "RestAPIBuilder";

    public static RestApi buildRetrofitService() {
        OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

        OkHttpClient.Builder builder = okHttpClient.newBuilder();
        builder.readTimeout(2, TimeUnit.MINUTES);
        builder.connectTimeout(2, TimeUnit.MINUTES);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        builder.addInterceptor(chain -> {
            Request request;
            String versionName = BuildConfig.VERSION_NAME;
            if (L.isLogin(OSSApplication.mContext)) {
                request = chain.request().newBuilder()
                        .addHeader("Authorization", Credentials.basic(BuildConfig.userId, BuildConfig.password))
                        .addHeader("X-ONESHOP-API-KEY", BuildConfig.apiKry)
                        .addHeader("X-ONESHOP-LOGIN-TOKEN", L.getAuthtoken(OSSApplication.mContext))
                        .addHeader("User-Id", L.getUser(OSSApplication.mContext).getId())
                        .build();

//                Log.d(TAG, "buildRetrofitService: -> " + L.getUser(OIGApplication.mContext).getId());
//                Log.d(TAG, "buildRetrofitService: -> " + L.getAuthtoken(OIGApplication.mContext));
            } else {

                request = chain.request().newBuilder()
                        .addHeader("Authorization", Credentials.basic(BuildConfig.userId, BuildConfig.password))
                        .addHeader("X-ONESHOP-API-KEY", BuildConfig.apiKry)
                        .build();
            }

            return chain.proceed(request);
        });

        OkHttpClient client = builder.build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.API_URL)
                .client(client).addConverterFactory(new ToStringConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create()).build();

        return retrofit.create(RestApi.class);
    }

}
