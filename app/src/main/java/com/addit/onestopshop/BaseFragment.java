package com.addit.onestopshop;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;

import com.addit.onestopshop.model.UserData;
import com.addit.onestopshop.network.RestAPIBuilder;
import com.addit.onestopshop.network.RestApi;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.Prefs;
import com.google.gson.Gson;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

public class BaseFragment extends Fragment {

    protected LinearLayoutManager layoutManager;

    protected RestApi restApi;
    public Prefs prefs;
    public UserData user;
    protected Gson gson;
    Dialog dialog;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutManager = new LinearLayoutManager(getActivity());
        prefs = Prefs.with(getActivity());
        restApi = RestAPIBuilder.buildRetrofitService();

        gson = new Gson();
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);
    }

    protected void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading...");
        }
        if (isShow) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}