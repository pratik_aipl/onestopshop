package com.addit.onestopshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.onestopshop.BaseActivity;
import com.addit.onestopshop.R;
import com.addit.onestopshop.adapter.adminOrderListAdpter;
import com.addit.onestopshop.listner.OrderStatusUpdate;
import com.addit.onestopshop.model.CutOrderListModel;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class AdminOrderDetailsList extends BaseActivity  implements OrderStatusUpdate {

    private static final String TAG = "ProductsItemList";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.lin_head)
    LinearLayout linHead;
    @BindView(R.id.recyclerlist)
    RecyclerView orderlist;
    adminOrderListAdpter adminOrderListAdpter;
    List<CutOrderListModel> cutOrderList =new ArrayList<>();
    CutOrderListModel cutOrderListModel;
    Subscription subscription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_order_list);
        ButterKnife.bind(this);

        tvTittle.setText("Order Details");
        cutOrderListModel = (CutOrderListModel) getIntent().getSerializableExtra(Constant.ORDERDETAILS);
        cutOrderList.add(cutOrderListModel);
        orderlist.setHasFixedSize(true);
        orderlist.setItemAnimator(new DefaultItemAnimator());
        orderlist.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        adminOrderListAdpter = new adminOrderListAdpter(this,cutOrderList,"Admin");
        orderlist.setAdapter(adminOrderListAdpter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }

    private void OrderStatusUpdate(String orderID, String STATUS) {
        Map<String, String> map = new HashMap<>();
        map.put("OrderID", orderID);
        map.put("Status", STATUS.equalsIgnoreCase("partially complete")?"partially_complete":STATUS);
        map.put("Products", gson.toJson(cutOrderList));
        Log.d(TAG, "placeOrder: " + gson.toJson(cutOrderList));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.OrderStatusUpdate(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if(jsonResponse.getBoolean("status")){
                        Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AdminOrderDetailsList.this,DashboardActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }else {
                        Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
    @Override
    public void onUpdatedCart(String orderID, String STATUS) {
        OrderStatusUpdate(orderID, STATUS);
    }
}