package com.addit.onestopshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.addit.onestopshop.BaseActivity;
import com.addit.onestopshop.R;
import com.addit.onestopshop.model.UserData;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.addit.onestopshop.utils.Validation;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ForgotPassword extends BaseActivity {

    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.btn_reset)
    Button btnLogin;
    @BindView(R.id.lin_login)
    LinearLayout linLogin;
    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_reset, R.id.lin_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_reset:
                if (!Validation.isValidEmail(L.getEditText(edtEmail))) {
                    edtEmail.setError("Please enter Valid Email");
                } else {
                    if (L.isNetworkAvailable(ForgotPassword.this))
                        ForgotPaswd();
                }
                break;
            case R.id.lin_login:
                startActivity(new Intent(ForgotPassword.this, LoginActivity.class));
                break;
        }
    }
    private void ForgotPaswd() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.EMAILID, L.getEditText(edtEmail));
        map.put(Constant.DeviceID, L.getDeviceId(this));
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.ForgotPasword(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, DashboardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

}
