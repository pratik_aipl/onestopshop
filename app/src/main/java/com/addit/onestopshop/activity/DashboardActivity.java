package com.addit.onestopshop.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.onestopshop.BaseActivity;
import com.addit.onestopshop.R;
import com.addit.onestopshop.fragment.AdminOrderListFragment;
import com.addit.onestopshop.fragment.DashbordFragment;
import com.addit.onestopshop.listner.CartCountEvent;
import com.addit.onestopshop.listner.OrderStatusUpdate;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.google.android.material.navigation.NavigationView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class DashboardActivity extends BaseActivity  implements OrderStatusUpdate {

    private static final String TAG = "DashboardActivity";
    @BindView(R.id.img_drawer)
    ImageView imgDrawer;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.rel)
    LinearLayout rel;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.framelayout)
    FrameLayout framelayout;
    @BindView(R.id.navigation)
    NavigationView navigation;
    @BindView(R.id.activity_main)
    DrawerLayout drawerLayout;

    ActionBarDrawerToggle t;

    TextView navUsername, navmobile;
    Subscription subscription;
    @BindView(R.id.tv_count)
    TextView tvCount;

    String CategoryID,
    SubCategoryID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        //For Cart count
        if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
            List<ItemListModel> productsListModels = new ArrayList<>(gson.fromJson(prefs.getString(Constant.cartData, ""), type));
            if (productsListModels.size() > 0) {
                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText(getString(R.string.qty, productsListModels.size()));
            } else
                tvCount.setVisibility(View.GONE);
        }

        drawerLayout.addDrawerListener(t);
        selectItem(0);


        View headerView = navigation.getHeaderView(0);
        navUsername = headerView.findViewById(R.id.tv_username);
        navmobile = headerView.findViewById(R.id.tv_usermob);


        navigation.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            switch (id) {
                case R.id.home:
                    selectItem(0);
                    break;
                case R.id.myorder:
                    selectItem(1);
                    break;
                    case R.id.cart:
                    selectItem(2);
                    break;
                case R.id.myproducts:
                    selectItem(3);
                    break;
                case R.id.changeprice:
                    selectItem(4);
                    break;
                    case R.id.tc:
                    selectItem(5);
                    break;
                case R.id.help:
                    selectItem(6);
                    break;
                case R.id.logout:
                    selectItem(7);
                    break;

                default:
                    break;
            }
            return true;

        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        navUsername.setText(user.getUsername());
        navmobile.setText(user.getEmail());
    }
    public void HideShow() {
        Menu nav_Menu = navigation.getMenu();
        if(user.getRole_id().equalsIgnoreCase("1")){
            nav_Menu.findItem(R.id.myorder).setVisible(false);
            nav_Menu.findItem(R.id.cart).setVisible(false);
            nav_Menu.findItem(R.id.tc).setVisible(false);
            nav_Menu.findItem(R.id.myproducts).setVisible(true);
            nav_Menu.findItem(R.id.changeprice).setVisible(true);
        }else{
            nav_Menu.findItem(R.id.myorder).setVisible(true);
            nav_Menu.findItem(R.id.cart).setVisible(true);
            nav_Menu.findItem(R.id.tc).setVisible(true);
            nav_Menu.findItem(R.id.myproducts).setVisible(false);
            nav_Menu.findItem(R.id.changeprice).setVisible(false);
        }

    }


    @OnClick({R.id.img_drawer, R.id.img_cart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_drawer:
                L.hideKeyboard(this, drawerLayout);
                drawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.img_cart:
                Log.d(TAG, "CART>>>> "+prefs.getString(Constant.cartData, ""));
                if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
                    List<ItemListModel> productsListModels = new ArrayList<>(gson.fromJson(prefs.getString(Constant.cartData, ""), type));
                    if (productsListModels.size() > 0) {
                        startActivity(new Intent(DashboardActivity.this, CartActivity.class));
                    } else {
                        Toast.makeText(this, "Cart is Empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "Cart is Empty", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CartCountEvent event) {
        //cartProductList.remove(event.getPos());
        if (event.getCount() > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(getString(R.string.qty, event.getCount()));
        } else {
            tvCount.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    private void selectItem(int position) {

        Fragment fragment = null;
        Bundle bundle = new Bundle();

        switch (position) {
            case 0:
                HideShow();
                if(user.getRole_id().equalsIgnoreCase("1")){
                    imgCart.setVisibility(View.GONE);
                    fragment = new AdminOrderListFragment();
                    fragment.setArguments(bundle);

                }else{
                    fragment = new DashbordFragment();
                    fragment.setArguments(bundle);
                }
                // tvDashboard.setText(getString(R.string.buyer_dashboard));
                break;
            case 1:
                drawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(DashboardActivity.this, CustomerOrderList.class));

                break;

                case 2:
                drawerLayout.closeDrawer(Gravity.LEFT);
                if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
                    List<ItemListModel> productsListModels = new ArrayList<>(gson.fromJson(prefs.getString(Constant.cartData, ""), type));
                    if (productsListModels.size() > 0) {
                        startActivity(new Intent(DashboardActivity.this, CartActivity.class));
                    } else {
                        Toast.makeText(DashboardActivity.this, "Cart is Empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(DashboardActivity.this, "Cart is Empty", Toast.LENGTH_SHORT).show();
                }
                break;
            case 3:
                drawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(DashboardActivity.this, AdminProductsItemList.class));
                break;
                case 4:
                drawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(DashboardActivity.this, TermsAndCondition.class));
                break;
                case 5:
                drawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(DashboardActivity.this, TermsAndCondition.class));
                break;
            case 6:
                drawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(DashboardActivity.this, HelpActivity.class));
                break;
            case 7:
                alert();
                break;

            default:
                break;
        }

        if (fragment != null) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.framelayout, fragment).commit();
        } else {
            Log.e("Dash", "Error in creating fragment");

        }
    }

    public void alert() {
        new AlertDialog.Builder(this)
                .setMessage((R.string.logout))
                .setPositiveButton("YES", (dialog, which) -> {
                    getLogout();
                })
                .setNegativeButton("NO", null)
                .show();
    }

    private void getLogout() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DeviceID, L.getDeviceId(DashboardActivity.this));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogout(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    L.logout(DashboardActivity.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void OrderStatusUpdate(String orderID, String STATUS) {
        Map<String, String> map = new HashMap<>();
        map.put("OrderID", orderID);
        map.put("Status", STATUS);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.OrderStatusUpdate(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if(jsonResponse.getBoolean("status")){
                        Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        selectItem(0);
                    }else {
                        Toast.makeText(this, ""+jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
    @Override
    public void onUpdatedCart(String orderID, String STATUS) {
        OrderStatusUpdate(orderID, STATUS);
    }

}
