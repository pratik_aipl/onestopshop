package com.addit.onestopshop.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.addit.onestopshop.BaseActivity;
import com.addit.onestopshop.R;
import com.addit.onestopshop.adapter.AdminProductItemListAdpter;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class AdminProductsItemList extends BaseActivity {

    private static final String TAG = "ProductsItemList";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.lin_head)
    LinearLayout linHead;
    @BindView(R.id.recyclerlist)
    RecyclerView recyclerlist;

    AdminProductItemListAdpter productItemListAdpter;
    Subscription subscription;

    List<ItemListModel> productList = new ArrayList<>();
    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list);
        ButterKnife.bind(this);

        tvTittle.setText("MY PRODUCTS");
        fabAdd.setVisibility(View.VISIBLE);

        if (L.isNetworkAvailable(this)) {
            ProductsList();
        }


        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    private void ProductsList() {
        Map<String, String> map = new HashMap<>();
        // map.put("category_id", CategoryID);
        // map.put("sub_category_id", SubCatID);
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.GetProducts(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        productList.clear();
                        productList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), ItemListModel.class));
                        productItemListAdpter = new AdminProductItemListAdpter(this, productList);
                        recyclerlist.setAdapter(productItemListAdpter);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
