package com.addit.onestopshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.addit.onestopshop.BaseActivity;
import com.addit.onestopshop.R;
import com.addit.onestopshop.adapter.DashboardItemList;
import com.addit.onestopshop.adapter.ProductListAdpater;
import com.addit.onestopshop.listner.CartCountEvent;
import com.addit.onestopshop.model.CatListModel;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.model.SubCatListModel;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ProductsList extends BaseActivity {

    private static final String TAG = "ProductsList";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.lin_head)
    LinearLayout linHead;
    @BindView(R.id.recyclerlist)
    RecyclerView recyclerlist;

    ProductListAdpater productListAdpater;

    String CategoryID;
    Subscription subscription;
    List<SubCatListModel> SubCatListModel=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_list);
        ButterKnife.bind(this);
        imgCart.setVisibility(View.VISIBLE);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        //For Cart count
        if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
            List<ItemListModel> productsListModels = new ArrayList<>(gson.fromJson(prefs.getString(Constant.cartData, ""), type));
            if (productsListModels.size() > 0) {
                tvCount.setVisibility(View.VISIBLE);
                tvCount.setText(getString(R.string.qty, productsListModels.size()));
            } else
                tvCount.setVisibility(View.GONE);
        }


        CategoryID=  getIntent().getStringExtra(Constant.CATEGORYID);
        tvTittle.setText(getIntent().getStringExtra(Constant.CATNAME));

        if (L.isNetworkAvailable(this)) {
            SubCategoryList();
        }

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new GridLayoutManager(this, 2));

        imgCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
                    List<ItemListModel> productsListModels = new ArrayList<>(gson.fromJson(prefs.getString(Constant.cartData, ""), type));
                    if (productsListModels.size() > 0) {
                        startActivity(new Intent(ProductsList.this, CartActivity.class));
                    } else {
                        Toast.makeText(ProductsList.this, "Cart is Empty", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ProductsList.this, "Cart is Empty", Toast.LENGTH_SHORT).show();
                }

            }
        });




    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void SubCategoryList() {
        Map<String, String> map = new HashMap<>();
        map.put("category_id", CategoryID);
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.GetSubCategory(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        SubCatListModel.clear();
                        SubCatListModel.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), SubCatListModel.class));
                        productListAdpater = new ProductListAdpater(this,SubCatListModel,CategoryID);
                        recyclerlist.setAdapter(productListAdpater);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CartCountEvent event) {
        //cartProductList.remove(event.getPos());
        Log.d(TAG, "onMessageEvent: " + event.getCount());
        if (event.getCount() > 0) {
            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(getString(R.string.qty, event.getCount()));
        } else {
            tvCount.setVisibility(View.GONE);
        }
    }
    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }
}
