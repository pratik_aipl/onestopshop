package com.addit.onestopshop.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.onestopshop.BaseActivity;
import com.addit.onestopshop.R;
import com.addit.onestopshop.adapter.CustOrderListAdpter;
import com.addit.onestopshop.adapter.ProductItemListAdpter;
import com.addit.onestopshop.listner.CartCountEvent;
import com.addit.onestopshop.model.CutOrderListModel;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerOrderList extends BaseActivity {

    private static final String TAG = "ProductsItemList";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.lin_head)
    LinearLayout linHead;
    @BindView(R.id.recyclerlist)
    RecyclerView recyclerlist;

    CustOrderListAdpter custOrderListAdpter;
    Subscription subscription;

    List<CutOrderListModel> cutOrderList =new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_order_list);
        ButterKnife.bind(this);

        tvTittle.setText("My Order");

        if (L.isNetworkAvailable(this)) {
            OrderList();
        }


        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));


    }

    private void OrderList() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.GetMyOrder(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        cutOrderList.clear();
                        cutOrderList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), CutOrderListModel.class));
                        custOrderListAdpter = new CustOrderListAdpter(this,cutOrderList);
                        recyclerlist.setAdapter(custOrderListAdpter);
                    }

                    if(cutOrderList.size()==0){
                        onBackPressed();
                        Toast.makeText(this, "Order Not Available", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }


}