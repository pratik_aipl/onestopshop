package com.addit.onestopshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.onestopshop.BaseActivity;
import com.addit.onestopshop.R;
import com.addit.onestopshop.adapter.CartProductsAdpater;
import com.addit.onestopshop.adapter.ProductItemListAdpter;
import com.addit.onestopshop.listner.CartCountEvent;
import com.addit.onestopshop.listner.GetUpdatedCart;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class CartActivity extends BaseActivity implements GetUpdatedCart {

    private static final String TAG = "CartActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.CartRecyclerList)
    RecyclerView CartRecyclerList;
    List<ItemListModel> detailsModelList = new ArrayList<>();


    CartProductsAdpater cartProductsAdpater;
    @BindView(R.id.tv_subTotal)
    TextView tvSubTotal;
    @BindView(R.id.tv_DelCharge)
    TextView tvDelCharge;
    @BindView(R.id.tv_Total)
    TextView tvTotal;
    @BindView(R.id.btn_placeOrder)
    Button btnPlaceOrder;

    Subscription subscription;
    double total = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);


        imgCart.setVisibility(View.VISIBLE);
        imgCart.setImageResource(R.drawable.plus_plus);
        tvTittle.setText("CART");
        if (!TextUtils.isEmpty(prefs.getString(Constant.cartData, ""))) {
            detailsModelList = gson.fromJson(prefs.getString(Constant.cartData, ""), type);
        }
        CartRecyclerList.setHasFixedSize(true);
        CartRecyclerList.setItemAnimator(new DefaultItemAnimator());
        CartRecyclerList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        cartProductsAdpater = new CartProductsAdpater(this, detailsModelList);
        CartRecyclerList.setAdapter(cartProductsAdpater);
        GetTotal();

    }

    @OnClick({R.id.img_back, R.id.img_cart, R.id.btn_placeOrder})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_cart:
                startActivity(new Intent(CartActivity.this, DashboardActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.btn_placeOrder:
                if (detailsModelList.size() != 0) {
                   // startActivity(new Intent(CartActivity.this, PlaceOrderActivity.class));
                    if (L.isNetworkAvailable(CartActivity.this))
                        placeOrder();
                } else {
                    Toast.makeText(this, "please Add Item", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onBackPressed() {
        prefs.save(Constant.cartData, gson.toJson(detailsModelList));
        super.onBackPressed();
    }

    @Override
    public void onUpdatedCart() {
      //  if(detailsModelList.size()!=0){
            GetTotal();
      /*  }else{
            onBackPressed();
        }*/
    }

    public void GetTotal(){
        total=0;
        for (int i = 0; i < detailsModelList.size(); i++) {
            if(detailsModelList.get(i).getIsPack().equalsIgnoreCase("0")){
                total = total + (Double.parseDouble(detailsModelList.get(i).getLPPrice()));
            }else{
                total = total + (Double.parseDouble(detailsModelList.get(i).getPrice()) * detailsModelList.get(i).getQty());
            }
        }
        tvSubTotal.setText(getString(R.string.total_display, "RS "+total));
        tvTotal.setText(getString(R.string.total_display, "RS "+total));

    }

    private void placeOrder() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.USERID, user.getId());
        map.put("SubTotal", String.valueOf(total));
        map.put("DeliveryCharge", "0.0");
        map.put("TotalPrice", String.valueOf(total));
        map.put("Products", gson.toJson(detailsModelList));
        Log.d(TAG, "placeOrder: " + gson.toJson(detailsModelList));

        showProgress(true);
        subscription= NetworkRequest.performAsyncRequest(restApi.PlaceOrder(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    prefs.save(Constant.cartData, "");
                    EventBus.getDefault().post(new CartCountEvent(0));
                    Toast.makeText(this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, ThankYouActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                            Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


}
