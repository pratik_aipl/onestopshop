package com.addit.onestopshop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.addit.onestopshop.BaseActivity;
import com.addit.onestopshop.R;
import com.addit.onestopshop.model.UserData;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.addit.onestopshop.utils.Validation;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_paswd)
    EditText edtPaswd;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.tv_forgot)
    TextView tvForgot;
    @BindView(R.id.lin_register)
    LinearLayout linRegister;

    Subscription subscription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.tv_forgot, R.id.btn_login, R.id.lin_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_forgot:
                startActivity(new Intent(LoginActivity.this, ForgotPassword.class));
                break;
            case R.id.btn_login:
                if (!Validation.isValidEmail(L.getEditText(edtEmail))) {
                    edtEmail.setError("Please enter Valid Email");
                } else if (Validation.isEmpty(L.getEditText(edtPaswd))) {
                    edtPaswd.setError("Please enter Password");
                } else {
                    if (L.isNetworkAvailable(LoginActivity.this))
                        callLogin();
                }
                break;
            case R.id.lin_register:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                break;
        }
    }
    private void callLogin() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.EMAILID, L.getEditText(edtEmail));
        map.put(Constant.Password, L.getEditText(edtPaswd));
        map.put(Constant.DeviceID, L.getDeviceId(this));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogin(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    prefs.save(Constant.loginAuthToken, DATA.getString("login_token"));
                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    prefs.save(Constant.isLogin, true);

                    Intent intent = new Intent(this, DashboardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


}
