package com.addit.onestopshop.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.addit.onestopshop.BaseActivity;
import com.addit.onestopshop.R;
import com.addit.onestopshop.adapter.CustOrderListAdpter;
import com.addit.onestopshop.adapter.adminOrderItemListAdpter;
import com.addit.onestopshop.adapter.adminOrderListAdpter;
import com.addit.onestopshop.model.CutOrderDetailsListModel;
import com.addit.onestopshop.model.CutOrderListModel;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class CustomerOrderDetailsList extends BaseActivity {

    private static final String TAG = "ProductsItemList";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_cart)
    ImageView imgCart;
    @BindView(R.id.tv_count)
    TextView tvCount;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.lin_head)
    LinearLayout linHead;
    @BindView(R.id.recyclerlist)
    RecyclerView orderlist;
    private adminOrderListAdpter adminOrderListAdpter;
    List<CutOrderListModel> cutOrderList =new ArrayList<>();
    CutOrderListModel cutOrderListModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_order_list);
        ButterKnife.bind(this);

        tvTittle.setText("Order Details");
        cutOrderListModel = (CutOrderListModel) getIntent().getSerializableExtra(Constant.ORDERDETAILS);
        cutOrderList.add(cutOrderListModel);
        orderlist.setHasFixedSize(true);
        orderlist.setItemAnimator(new DefaultItemAnimator());
        orderlist.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));

        adminOrderListAdpter = new adminOrderListAdpter(this,cutOrderList,"Customer");
        orderlist.setAdapter(adminOrderListAdpter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.img_back)
    public void onViewClicked() {
        onBackPressed();
    }

}