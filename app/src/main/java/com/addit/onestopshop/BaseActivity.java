package com.addit.onestopshop;

import android.app.ProgressDialog;
import android.os.Bundle;


import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.model.UserData;
import com.addit.onestopshop.network.RestAPIBuilder;
import com.addit.onestopshop.network.RestApi;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.Prefs;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.lang.reflect.Type;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public class BaseActivity extends AppCompatActivity {

    protected RestApi restApi;
    public Prefs prefs;
    public UserData user;
    protected Gson gson;
    public Type type;

    ProgressDialog progressDialog;
    public RxPermissions rxPermissions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxPermissions = new RxPermissions(this);
        restApi = RestAPIBuilder.buildRetrofitService();
        prefs = Prefs.with(this);
        gson = new Gson();
        type = new TypeToken<List<ItemListModel>>() {
        }.getType();
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);
    }

    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
        }
        if (isShow) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}
