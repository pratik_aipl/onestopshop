package com.addit.onestopshop.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import com.addit.onestopshop.BaseFragment;
import com.addit.onestopshop.R;
import com.addit.onestopshop.activity.DashboardActivity;
import com.addit.onestopshop.activity.ProductsList;
import com.addit.onestopshop.activity.SearchProductsActivity;
import com.addit.onestopshop.activity.SpecialOfferProductsList;
import com.addit.onestopshop.adapter.DashboardItemList;
import com.addit.onestopshop.adapter.ProductItemListAdpter;
import com.addit.onestopshop.model.CatListModel;
import com.addit.onestopshop.model.ItemListModel;
import com.addit.onestopshop.model.UserData;
import com.addit.onestopshop.network.NetworkRequest;
import com.addit.onestopshop.utils.Constant;
import com.addit.onestopshop.utils.L;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;
import rx.Subscription;
import rx.functions.Action1;

public class DashbordFragment extends BaseFragment {

    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.lin_search)
    LinearLayout linSearch;
    @BindView(R.id.lin_spcloffer)
    LinearLayout lin_spcloffer;
    @BindView(R.id.root_layout)
    LinearLayout root_layout;
    @BindView(R.id.singlerecyclerlist)
    RecyclerView singlerecyclerlist;


    private Unbinder unbinder;
    private DashboardItemList singleItemadpater;
    private Subscription subscription;

    private View view;
    private ArrayList<ItemListModel> productList=new ArrayList<>();

    private List<CatListModel> CatListModel=new ArrayList<>();
    public DashbordFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_dashbord, container, false);
        unbinder = ButterKnife.bind(this, view);
        lin_spcloffer.setVisibility(View.VISIBLE);
        if (L.isNetworkAvailable(getActivity())) {
            CategoryList();
        }


        singlerecyclerlist.setHasFixedSize(true);
        singlerecyclerlist.setItemAnimator(new DefaultItemAnimator());
        singlerecyclerlist.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               if (!TextUtils.isEmpty(query))
                    getProducts(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        lin_spcloffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SpecialOfferProductsList.class));
            }
        });
        return view;
    }

    private void getProducts(String query) {
        showProgress(true);
        Map<String, String> map = new HashMap<>();
        map.put("search", query);
        subscription = NetworkRequest.performAsyncRequest(restApi.GetSearchProduct(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if (jsonResponse.has(Constant.data)) {
                        productList.clear();
                        productList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), ItemListModel.class));
                        getActivity().startActivity(new Intent(getActivity(), SearchProductsActivity.class)
                                .putExtra(Constant.PRODUCTLIST, productList));
                    }else {
                        L.generalOkAlert(getActivity(), jsonResponse.getString("message"), null);
                        // Toast.makeText(getActivity(), jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getActivity(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


    private void CategoryList() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.GetCategory(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        CatListModel.clear();
                        CatListModel.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), CatListModel.class));
                        singleItemadpater = new DashboardItemList(getActivity(), CatListModel);
                        singlerecyclerlist.setAdapter(singleItemadpater);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getActivity(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        L.hideKeyboard(getActivity(), searchView);
        searchView.clearFocus();
        searchView.setQuery("", false);
        root_layout.requestFocus();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
