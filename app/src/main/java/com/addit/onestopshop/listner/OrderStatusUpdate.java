package com.addit.onestopshop.listner;

public interface OrderStatusUpdate {
    void onUpdatedCart(String orderID, String STATUS);
}