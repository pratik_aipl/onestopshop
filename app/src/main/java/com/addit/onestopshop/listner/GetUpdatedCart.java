package com.addit.onestopshop.listner;

public interface GetUpdatedCart {
    void onUpdatedCart();
}